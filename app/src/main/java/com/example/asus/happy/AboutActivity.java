package com.example.asus.happy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import static com.example.asus.happy.service.address.Address.DOWNLOADURL;

/**
 * Created by asus on 2017/6/28.
 */

public class AboutActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView shareTV,backTV;

    public void init(){
        shareTV = (TextView)findViewById(R.id.shareTV);
        backTV = (TextView)findViewById(R.id.backTV);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        init();
        //分享
        shareTV.setOnClickListener(this);
        //返回
        backTV.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.shareTV:
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, DOWNLOADURL);
                shareIntent.setType("text/plain");
                //设置分享列表的标题，并且每次都显示分享列表
                startActivity(Intent.createChooser(shareIntent, "分享到"));
                break;
            case R.id.backTV:
                finish();
                break;
        }
    }
}
