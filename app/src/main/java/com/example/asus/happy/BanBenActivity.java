package com.example.asus.happy;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import static com.example.asus.happy.service.address.Address.BanBenADDRESS;

/**
 * Created by asus on 2017/6/28.
 */

public class BanBenActivity extends AppCompatActivity {
    private WebView webView;

    private void init(){
        webView = (WebView) findViewById(R.id.webView);
        //去除边框
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        //WebView加载web资源
        webView.loadUrl(BanBenADDRESS);
        //覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // TODO Auto-generated method stub
                //返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
                view.loadUrl(url);
                return true;
            }
        });

        //启用支持javascript
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.banben);

        init();
    }
}
