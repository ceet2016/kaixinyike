package com.example.asus.happy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.asus.happy.service.application.MyApplication;
import com.example.asus.happy.service.finaldata.FinalData;

public class MainActivity extends FragmentActivity implements View.OnClickListener{

    private FragmentTransaction ft;
    private FragmentManager fm;
    private SmileActivity smiFrag;
    private MyHomeActivity homeFrag;
    private Intent intent;
    private Button addBtn,myAlveoleBtn,smileBtn;
    private ProgressDialog dialog;//加载框


    //初始化控件的方法
    public void init(){
        fm = getSupportFragmentManager(); //获取fragmentmanager
        ft = fm.beginTransaction(); //事物

        smiFrag = new SmileActivity();
        homeFrag = new MyHomeActivity();

        addBtn = (Button)findViewById(R.id.addBtn);
        myAlveoleBtn = (Button)findViewById(R.id.myAlveoleBtn);
        smileBtn = (Button)findViewById(R.id.smileBtn);

        //发布笑话，添加按钮
        addBtn.setOnClickListener(this);
        //我的窝按钮
        myAlveoleBtn.setOnClickListener(this);
        //笑一笑按钮
        smileBtn.setOnClickListener(this);

        //初始化Fragment
        ft.replace(R.id.frag,smiFrag);
        ft.commit();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //调用初始化方法
        init();
    }

    @Override
    public void onClick(View view) {
        ft = fm.beginTransaction();
        if (R.id.addBtn == view.getId()){
            //发布笑话，跳转
            intent = new Intent(MainActivity.this,SendActivity.class);
            startActivity(intent);
        }else if(R.id.myAlveoleBtn == view.getId()){
            dialog = ProgressDialog.show(MainActivity.this, "", FinalData.LOADING, true, true);
            //小窝
            /**
             * 点击我的窝，需要判断用户是否登录了，（判断application中的userName是否为null(默认值为null)）
             * 如果application中userName不为空，说明用户已经登录，否则用户没有登录
             *
             */
            //获得application中的userName
            String userName = getApp().getUserName();
            //Toast.makeText(this, userName, Toast.LENGTH_SHORT).show();
            //判断userName是否为null
            if ("null".equals(userName)){
                Toast.makeText(this, FinalData.NOLOGIN, Toast.LENGTH_SHORT).show();
                //跳转到登录界面
                intent = new Intent(this,LoginActivity.class);
                startActivity(intent);
            }else {
                //跳转到我的窝
                ft.replace(R.id.frag,homeFrag);
            }
            dialog.dismiss();//关闭加载框
        }else if (R.id.smileBtn == view.getId()){
            //笑一笑
            ft.replace(R.id.frag,smiFrag);
        }
        ft.commit();
    }

    //回到该页面调用
    protected void onResume() {
        super.onResume();
        init();
    }

    //获得application方法
    public MyApplication getApp(){
        return (MyApplication) getApplicationContext();
    }
}