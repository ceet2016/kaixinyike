package com.example.asus.happy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.asus.happy.service.application.MyApplication;
import com.example.asus.happy.service.finaldata.FinalData;

public class SettingsActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener,View.OnClickListener{

    private Intent intent;
    private Button nightSwitch;
    private Button nightSwitch2;
    private Button loginOutBtn;
    private LinearLayout banbengenxin;
    private LinearLayout yijianfankui;
    private LinearLayout changjianwenti;
    private LinearLayout aboutUsLL;


    public void init(){
        nightSwitch = (Button) findViewById(R.id.nightSwitch);
        nightSwitch2 = (Button) findViewById(R.id.nightSwitch2);
        loginOutBtn = (Button)findViewById(R.id.loginOutBtn);
        banbengenxin = (LinearLayout) findViewById(R.id.banbengenxin);
        yijianfankui = (LinearLayout) findViewById(R.id.yijianfankui);
        changjianwenti = (LinearLayout) findViewById(R.id.changjianwenti);
        aboutUsLL = (LinearLayout) findViewById(R.id.aboutUsLL);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        //初始化控件
        init();
        //switch监听
        nightSwitch.setOnClickListener(this);
        nightSwitch2.setOnClickListener(this);
        //退出登录
        loginOutBtn.setOnClickListener(this);
        // 版本更新
        banbengenxin.setOnClickListener(this);
        // 意见反馈
        yijianfankui.setOnClickListener(this);
        // 常见问题
        changjianwenti.setOnClickListener(this);
        // 关于我们
        aboutUsLL.setOnClickListener(this);
    }

    //监听Switch选项改变事件
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        switch (compoundButton.getId()){

        }

}

    //按钮点击事件
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.loginOutBtn:
                getApp().setUserName(FinalData.NULL);
                intent = new Intent(this,LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.banbengenxin:
                intent = new Intent(this,BanBenActivity.class);
                startActivity(intent);
                break;
            case R.id.yijianfankui:
                intent = new Intent(this,YiJianActivity.class);
                startActivity(intent);
                break;
            case R.id.changjianwenti:
                intent = new Intent(this,WenTiActivity.class);
                startActivity(intent);
                break;
            case R.id.aboutUsLL:
                intent = new Intent(this,AboutActivity.class);
                startActivity(intent);
                break;
            case R.id.nightSwitch:
                Toast.makeText(this, "已开启自动清理缓存", Toast.LENGTH_SHORT).show();
                nightSwitch.setEnabled(false);
                nightSwitch2.setEnabled(true);
                break;
            case  R.id.nightSwitch2:
                Toast.makeText(this, "已关闭自动清理缓存", Toast.LENGTH_SHORT).show();
                nightSwitch.setEnabled(true);
                nightSwitch2.setEnabled(false);
                break;
        }
    }

    //获取application
    public MyApplication getApp(){
        return (MyApplication) getApplicationContext();
    }
}
