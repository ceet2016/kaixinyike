package com.example.asus.happy.service.application;

import android.app.Application;

/**
 * Created by asus on 2017/6/22.
 */

public class MyApplication extends Application{
    private static String userName = "null";//用户名，默认为null,说明用户没有登录

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
