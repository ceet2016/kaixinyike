package com.example.asus.happy.service.finaldata;

import java.io.Serializable;

/**
 * Created by asus on 2017/6/27.
 */

public class FinalData implements Serializable{

    public static final int ZERO_INT = 0;
    public static final String ZERO_STRING = "0";
    public static final String NULL = "null";
    public static final String ONE_STRING = "1";
    public static final int ONE_INT = 1;
    public static final int TWO_HUNDRED = 200;
    public static final String CODE = "UTF-8";
    public static final String LOADING = "加载中...";
    public static final String NOLOGIN = "你还没有登录，请登录!";
    public static final String LAUD = "点赞";
    public static final String LEFT = "(";
    public static final String RIGHT = ")";
    public static final String DISCUSS = "评论";
    public static final String NODISCUSS = "请输入评论内容！";
    public static final String DISCUSS_REFRESH = "评论已刷新！";
    public static final String DISCUSS_ADD_SUCC = "评论添加成功！正在后台进行审核......";
    public static final String NONETWORK = "网络不流畅......";
    public static final String LOGIN_SUCC = "登录成功!";
    public static final String LOGIN_FAIL = "帐号或密码错误!";
    public static final String LOGINING = "登录中...";
    public static final String REGISTER_SUCC = "注册成功，你可以登录了!";
    public static final String REGISTER_FAIL = "该帐号已经被注册了，请更换!";
    public static final String NUMBER_PWD_NOTNULL = "帐号或密码不能为空!";
    public static final int RESULT_LOAD_IMAGE = 1;
    public static final String ADD_SUCC = "发布成功!";
    public static final String NOCONTENT = "你还没有输入内容呢!";
    public static final String JOCK_REFRESH = "笑话已刷新!";
}
