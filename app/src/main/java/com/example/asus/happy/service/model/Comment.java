package com.example.asus.happy.service.model;

import java.io.Serializable;

public class Comment implements Serializable {

	private static final long serialVersionUID = 1L;
	private int m_id;
	private String m_ct;
	private int c_id;
	private String other;
	private int u_id;
	private String c_ct;
	private int c_num;
	private int c_sum;
	private String u_userName;

	public String getU_userName() {
		return u_userName;
	}

	public void setU_userName(String u_userName) {
		this.u_userName = u_userName;
	}

	public int getU_id() {
		return u_id;
	}

	public void setU_id(int u_id) {
		this.u_id = u_id;
	}

	public String getC_ct() {
		return c_ct;
	}

	public void setC_ct(String c_ct) {
		this.c_ct = c_ct;
	}

	public int getC_num() {
		return c_num;
	}

	public void setC_num(int c_num) {
		this.c_num = c_num;
	}

	public int getC_sum() {
		return c_sum;
	}

	public void setC_sum(int c_sum) {
		this.c_sum = c_sum;
	}

	public int getM_id() {
		return m_id;
	}

	public void setM_id(int m_id) {
		this.m_id = m_id;
	}

	public String getM_ct() {
		return m_ct;
	}

	public void setM_ct(String m_ct) {
		this.m_ct = m_ct;
	}

	public int getC_id() {
		return c_id;
	}

	public void setC_id(int c_id) {
		this.c_id = c_id;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public Comment(int m_id, String m_ct, int c_id, String other) {
		super();
		this.m_id = m_id;
		this.m_ct = m_ct;
		this.c_id = c_id;
		this.other = other;
	}

	public Comment() {
		super();
	}

	@Override
	public String toString() {
		return "Comment [m_id=" + m_id + ", m_ct=" + m_ct + ", c_id=" + c_id + ", other=" + other + "]";
	}

}