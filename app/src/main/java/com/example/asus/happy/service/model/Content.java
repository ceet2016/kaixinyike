package com.example.asus.happy.service.model;

import java.io.Serializable;

public class Content implements Serializable {

	private static final long serialVersionUID = 1L;
	private int c_id;
	private int u_id;
	private String c_ct;
	private int c_num;
	private int c_sum;

	public int getC_id() {
		return c_id;
	}

	public void setC_id(int c_id) {
		this.c_id = c_id;
	}

	public int getU_id() {
		return u_id;
	}

	public void setU_id(int u_id) {
		this.u_id = u_id;
	}

	public String getC_ct() {
		return c_ct;
	}

	public void setC_ct(String c_ct) {
		this.c_ct = c_ct;
	}

	public int getC_num() {
		return c_num;
	}

	public void setC_num(int c_num) {
		this.c_num = c_num;
	}

	public int getC_sum() {
		return c_sum;
	}

	public void setC_sum(int c_sum) {
		this.c_sum = c_sum;
	}

	public Content(int c_id, int u_id, String c_ct, int c_num, int c_sum) {
		super();
		this.c_id = c_id;
		this.u_id = u_id;
		this.c_ct = c_ct;
		this.c_num = c_num;
		this.c_sum = c_sum;
	}

	public Content() {
		super();
	}

	@Override
	public String toString() {
		return "Content [c_id=" + c_id + ", u_id=" + u_id + ", c_ct=" + c_ct + ", c_num=" + c_num + ", c_sum=" + c_sum
				+ "]";
	}

	private String u_userName;
	private String u_passWord;
	private String u_Name;
	private String u_token;
	private String other;

	public String getU_userName() {
		return u_userName;
	}

	public void setU_userName(String u_userName) {
		this.u_userName = u_userName;
	}

	public String getU_passWord() {
		return u_passWord;
	}

	public void setU_passWord(String u_passWord) {
		this.u_passWord = u_passWord;
	}

	public String getU_Name() {
		return u_Name;
	}

	public void setU_Name(String u_Name) {
		this.u_Name = u_Name;
	}

	public String getU_token() {
		return u_token;
	}

	public void setU_token(String u_token) {
		this.u_token = u_token;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

}
