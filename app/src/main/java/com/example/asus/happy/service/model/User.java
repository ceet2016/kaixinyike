package com.example.asus.happy.service.model;

import java.io.Serializable;

/**
 * 用户表，前台后台通用
 * @author asus
 *
 */
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	private int u_id;
	private String u_userName;
	private String u_passWord;
	private String u_Name;
	private String u_token;
	private String other;
	public int getU_id() {
		return u_id;
	}
	public void setU_id(int u_id) {
		this.u_id = u_id;
	}
	public String getU_userName() {
		return u_userName;
	}
	public void setU_userName(String u_userName) {
		this.u_userName = u_userName;
	}
	public String getU_passWord() {
		return u_passWord;
	}
	public void setU_passWord(String u_passWord) {
		this.u_passWord = u_passWord;
	}
	public String getU_Name() {
		return u_Name;
	}
	public void setU_Name(String u_Name) {
		this.u_Name = u_Name;
	}
	public String getU_token() {
		return u_token;
	}
	public void setU_token(String u_token) {
		this.u_token = u_token;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}
	public User(int u_id, String u_userName, String u_passWord, String u_Name, String u_token, String other) {
		super();
		this.u_id = u_id;
		this.u_userName = u_userName;
		this.u_passWord = u_passWord;
		this.u_Name = u_Name;
		this.u_token = u_token;
		this.other = other;
	}
	public User() {
		super();
	}
	@Override
	public String toString() {
		return "User [u_id=" + u_id + ", u_userName=" + u_userName + ", u_passWord=" + u_passWord + ", u_Name=" + u_Name
				+ ", u_token=" + u_token + ", other=" + other + "]";
	}
	
}
